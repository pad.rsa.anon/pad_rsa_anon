#!/usr/bin/python

import hashlib

from libPadRSA.consts import Grsa2048
from libPadRSA.defs import Defs
import libPadRSA.group_ops as lgops
import libPadRSA.prng as lprng
import libPadRSA.util as lu

class GooSigTokGen(object):
    def __init__(self, gops):
        self.gops = gops if gops is not None else lgops.RSAGroupOps(Grsa2048, Defs.max_rsa_keysize)

    def send_tokens(self, rsapubkey):
        # NOTE: select a 256-bit s' and expand to 2048-bit s, e.g., with AESEnc(s', 0), ..., AESEnc(s', 3)
        s_prime = lu.rand.getrandbits(256)
        s = lprng.expand_sprime(s_prime)

        # the challenge: a commitment to the RSA modulus
        C1 = self.gops.reduce(self.gops.powgh(rsapubkey.n, s))
        hC1 = int(hashlib.sha256(str(C1).encode("utf-8")).hexdigest(), 16)

        # (Hash(C1) || s_prime), encrypted to the pubkey
        C0 = rsapubkey.encrypt((hC1 << 256) | s_prime)

        return (C0, C1)
