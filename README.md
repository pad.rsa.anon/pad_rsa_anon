# libPadRSA

libPadRSA is written in Python. It is compatible with CPython 2 and 3, PyPy, and PyPy3.

## running

You can run a few tests with

    python -O -m ligPadRSA [-a] [niters]
    # or python2, python3, pypy, pypy3, ...

`-a` enables all tests (otherwise only end-to-end tests run). `niters` sets the number of test iterations; 16 is the default.

## License

libPadRSA is (C) Anonymous Authors.

This software is for review only. Please do not distribute.
